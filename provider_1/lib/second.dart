import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_1/list_provider.dart';

class Second extends StatelessWidget {
  const Second({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<NumberProvider>(
      builder: (context, numberProvider, child) => Scaffold(
        appBar: AppBar(),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            numberProvider.add();
          },
          child: Icon(Icons.add),
        ),
        body: SizedBox(
          child: Column(
            children: [
              Text(numberProvider.num.last.toString()),
              Container(
                height: 200,
                width: double.maxFinite,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: numberProvider.num.length,
                  itemBuilder: (context, index) {
                    return Text(numberProvider.num[index].toString());
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
