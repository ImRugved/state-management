import 'package:flutter/material.dart';

class NumberProvider extends ChangeNotifier {
  List<int> num = [1, 2, 3, 4];
  void add() {
    int last = num.last;
    num.add(last + 1);
    notifyListeners();
  }
}
