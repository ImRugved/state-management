import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:provider_1/list_provider.dart';
import 'package:provider_1/second.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<NumberProvider>(
      builder: (context, numberProvider, child) => Scaffold(
        appBar: AppBar(),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            numberProvider.add();
          },
          child: Icon(Icons.add),
        ),
        body: SizedBox(
          child: Column(
            children: [
              Text(numberProvider.num.last.toString()),
              Expanded(
                child: ListView.builder(
                  itemCount: numberProvider.num.length,
                  itemBuilder: (context, index) {
                    return Text(numberProvider.num[index].toString());
                  },
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => Second(),
                  ));
                },
                child: Text('second'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
